# Octave

You can also create content with Jupyter Notebooks. This means that you can include
code blocks and their outputs in your book.

x=0:0.001:2*pi;

plot(x,sin(x))