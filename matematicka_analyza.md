# Matematická analýza

$$\lim_{n \rightarrow \infty} (7 n^4-8 n^3+3n-1)=$$


## vypočítej

```{toggle}
$$\lim_{n \rightarrow \infty} (7 n^4-8 n^3+3n-1)=\lim_{n \rightarrow \infty} n^4 \left(7- 8\frac{1}{n}+3 \frac{1}{n^3}-1\frac{1}{n^4}\right)=\lim_{n \rightarrow \infty} n^4 \left ( \lim_{n \rightarrow \infty} 7-8\lim_{n \rightarrow \infty} \frac{1}{n}+3\lim_{n \rightarrow \infty} \frac{1}{n^3}-\lim_{n \rightarrow \infty} \frac{1}{n^4}\right )$$
```


```{admonition} Klikni pro zobrazení řešení
:class: dropdown
$$\lim_{n \rightarrow \infty} (7 n^4-8 n^3+3n-1)=\lim_{n \rightarrow \infty} n^4 \left(7- 8\frac{1}{n}+3 \frac{1}{n^3}-1\frac{1}{n^4}\right)=\lim_{n \rightarrow \infty} n^4 \left ( \lim_{n \rightarrow \infty} 7-8\lim_{n \rightarrow \infty} \frac{1}{n}+3\lim_{n \rightarrow \infty} \frac{1}{n^3}-\lim_{n \rightarrow \infty} \frac{1}{n^4}\right )$$
```